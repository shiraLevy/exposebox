﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exposebox.Models
{
    public class UserDetails
    {
        public int Record_id { get; set; }
        public string Email { get; set; }
        public string Guest_id { get; set; }
        public string First_Name { get; set; }
        public string Account_id { get; set; }
        public string Date_of_Birth { get; set; }
        public string Anniversary { get; set; }
        public string Agree_to_SMS { get; set; }
        public string Agree_to_Email { get; set; }
        public string Shamaim_registration { get; set; }
        public string Partner_First_Name { get; set; }
        public string Partner_Last_Name { get; set; }
        public string Partnet_Date_of_Birth { get; set; }
        public string Partner_id { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string City { get; set; }
        public string Yearly_Hotel_Visits { get; set; }
        public string Annual_Revenue { get; set; }
        public string Club_Member_Number { get; set; }
        public string End_Date { get; set; }
        public string First_Enrollment_Date { get; set; }
        public string Enrollment_Date { get; set; }
        public string Renew_Date { get; set; }
        public string Guest_Rating { get; set; }
        public string Membership_Status { get; set; }

        //public string Lname { get; set; }
        //public string Passport { get; set; }
        //public string SpouseGender { get; set; }
        //public string WebPass { get; set; }

        public List<ResDetails> ResDetails { get; set; }
    }

    public class ResDetails
    {
        public string Master { get; set; }
        public string Extra_Income { get; set; }
        public string Gross_Room_Income { get; set; }
        public string Adults { get; set; }
        public string Childrens { get; set; }
        public string Babies { get; set; }
        public string Hotel { get; set; }
        public string Hotel_Name { get; set; }
        public string Plan { get; set; }
        public string Room_Nights { get; set; }
        public string Room_Category { get; set; }
        public string Reservation_Status { get; set; }
        public string Arrival_Date { get; set; }
        public string Depart_Date { get; set; }
        public string Taken_Date { get; set; }
        public string Nationality { get; set; }

        //public string PriceCode { get; set; }
        //public string Res { get; set; }
        //public string AgentName { get; set; }
        //public string TakenClerkName { get; set; }
        //public string MarketSegmentDescription { get; set; }
        //public string CompanyName { get; set; }
        //public string RoomNumber { get; set; }
        //public string RecordType { get; set; }
        //public string GuestCategoryDescription { get; set; }
        //public string LocalTourist { get; set; }
        //public string CancellationDate { get; set; }
    }
}