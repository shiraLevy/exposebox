﻿using Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exposrbox
{
    class BaseLogger
    {
        public LoggerService ApplicationLogger;
        public string DBLocal = @"provider=SQLOLEDB;Data Source=FT-VISUAL01\SQLEXPRESS;Initial Catalog=ExposeBox; User ID=zigit;Password=Zigit#1";
        public string minLogLevel = "3";

        public BaseLogger()
        {
            ApplicationLogger = new LoggerService(DBLocal, (LoggerService.LogLevel)int.Parse(minLogLevel));
        }

        public void CreateLogLevel(string id, string currentMethod, string message, LoggerService.ActionType action, string extras = "", string ip = "")
        {
            ApplicationLogger.CreateLog(id, LoggerService.LogLevel.Log, currentMethod, message, action, false, ip, extras);
        }

        public void CreateErrorLevel(string id, string currentMethod, string message, LoggerService.ActionType action = LoggerService.ActionType.Null, string extras = "", string ip = "")
        {
            ApplicationLogger.CreateLog(id, LoggerService.LogLevel.Error, currentMethod, message, action, true, ip, extras);
        }
    }
}
