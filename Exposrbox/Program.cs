﻿using Exposebox.Services;
using Newtonsoft.Json;

namespace Exposebox
{
    public class Program
    {
        static void Main()
        {
            SqlService SQLService = new SqlService();
            ChangingFormat date = new ChangingFormat();
            EmailService emailService = new EmailService();

            string ans;
            try
            {
                SQLService.GetDetails();
            }
            catch
            {
                // return Connection  error 
                ans = JsonConvert.SerializeObject(new
                {
                    Code = "500",
                    Message = "Connection Error"
                });
            }

            ans = JsonConvert.SerializeObject(SQLService.usersDetails);
            emailService.SendingEmail("shira.l@zigit.co.il", SQLService.count);
        }  
    }
}
