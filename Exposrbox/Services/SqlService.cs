﻿using Exposebox.Models;
using Exposrbox;
using Logger;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exposebox.Services
{
    class SqlService : BaseLogger
    {
        readonly OdbcConnection dbConnMaindb = new OdbcConnection("DSN=maindb;UID=dba;PWD=spoon");
        readonly OdbcConnection dbConnFattalStats = new OdbcConnection("Provider=SAOLEDB;DSN=fattal_stats");
        OdbcDataReader dbReaderMDB = null;
        OdbcDataReader dbReaderFS = null;
        Dictionary<string, string> Emails = new Dictionary<string, string>();
        ChangingFormat date = new ChangingFormat();
        FileService file = new FileService();

        public List<UserDetails> usersDetails = new List<UserDetails>();
        public int count = 1;

        public void GetDetails()
        {
            string CurrentMethod = "GetDetails";
            dbConnMaindb.Open();
            try {
                GetEmails();
                foreach (var emailUser in Emails)
                {
                    if (count == 55)
                    {
                        break;
                    }
                    UserDetails userDetails;
                    try {
                        userDetails = GetUserDetails(emailUser, count);
                    } catch {
                        continue;
                    }

                    try {
                        GetReservationsDetails(emailUser, userDetails);
                    } catch { }

                    usersDetails.Add(userDetails);
                    count++;
                }
            } catch (Exception ex) {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbConnMaindb.Close();
            }
            CreateLogLevel("", CurrentMethod, file.WriteLogFile(JsonConvert.SerializeObject(usersDetails)), LoggerService.ActionType.Null, "Result");
        }

        private void GetEmails()
        {
            string CurrentMethod = "GetEmails";
            var userRes = new OdbcCommand(GetUpdatedAccountsByUpdateDate(DateTime.Now, DateTime.Now.AddDays(1)))
            {
                Connection = dbConnMaindb
            };

            try
            {
                dbReaderMDB = userRes.ExecuteReader();
                RegexUtilities reg = new RegexUtilities();
                while (dbReaderMDB.Read())
                {
                    string gid = dbReaderMDB["c_guest_id"].ToString();
                    string email = dbReaderMDB["c_email"].ToString();
                    if (email.Length > 0 || reg.IsValidEmail(email) == true || gid.Length > 0)
                    {
                        try
                        {
                            Emails.Add(gid, email);
                        }
                        catch
                        {
                            // dont add email, Usually because of duplication id
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Could not get the email list
                CreateErrorLevel("", CurrentMethod, String.Format("dates: {0} - {1}", DateTime.Now, DateTime.Now.AddDays(1)), LoggerService.ActionType.Request);
                CreateErrorLevel("", CurrentMethod, ex.Message, LoggerService.ActionType.Response, "Connection failed");
                throw new Exception(ex.Message);
            }
        }

        private UserDetails GetUserDetails(KeyValuePair<string, string> emailUser, int count)
        {
            string CurrentMethod = "GetUserDetails";
            OdbcCommand cmd = null;
            cmd = new OdbcCommand(GetContactByGuestIdAndEmail(emailUser.Key, emailUser.Value))
            {
                Connection = dbConnMaindb
            };
            if (cmd.Connection.State == ConnectionState.Closed)
            {
                cmd.Connection.Open();
            }
            UserDetails userDetails = new UserDetails();

            try
            {
                dbReaderMDB = cmd.ExecuteReader();
                userDetails.Record_id = count;
                userDetails.Email = emailUser.Value;
                userDetails.Guest_id = emailUser.Key;
                while (dbReaderMDB.Read())  // run one time
                {
                    userDetails.First_Name = dbReaderMDB["first_name"].ToString();
                    userDetails.Account_id = dbReaderMDB["account_id"].ToString();
                    userDetails.Date_of_Birth = dbReaderMDB["date_of_birth"].ToString();
                    userDetails.Anniversary = dbReaderMDB["anniversary"].ToString();
                    userDetails.Agree_to_SMS = dbReaderMDB["allow_sms_marketing"].ToString();
                    userDetails.Agree_to_Email = dbReaderMDB["allow_email_marketing"].ToString();
                    userDetails.Shamaim_registration = dbReaderMDB["shamaim_registration"].ToString();
                    userDetails.Partner_First_Name = dbReaderMDB["spouse_name"].ToString();
                    userDetails.Partner_Last_Name = dbReaderMDB["spouse_last_name"].ToString();
                    userDetails.Partnet_Date_of_Birth = dbReaderMDB["spouse_date_of_birth"].ToString();
                    userDetails.Partner_id = dbReaderMDB["spouse_id"].ToString();
                    userDetails.Phone = dbReaderMDB["phone"].ToString();
                    userDetails.Mobile = dbReaderMDB["mobile"].ToString().Replace("-", "");
                    userDetails.City = dbReaderMDB["city"].ToString();
                    userDetails.Yearly_Hotel_Visits = dbReaderMDB["count_orders_last_year"].ToString();
                    userDetails.Annual_Revenue = dbReaderMDB["total_income_last_year"].ToString();
                    userDetails.Club_Member_Number = dbReaderMDB["club_member_number"].ToString();
                    userDetails.End_Date = dbReaderMDB["end_date"].ToString();
                    userDetails.First_Enrollment_Date = dbReaderMDB["first_enrollment_date"].ToString();
                    userDetails.Renew_Date = dbReaderMDB["renew_date"].ToString();
                    userDetails.Enrollment_Date = dbReaderMDB["enrollment_date"].ToString();
                    userDetails.Guest_Rating = dbReaderMDB["guest_rating"].ToString();
                    userDetails.Membership_Status = dbReaderMDB["membership_status"].ToString();
                    //userDetails.Lname = dbReaderMDB["last_name"].ToString();
                    //userDetails.SpouseGender = dbReaderMDB["spouse_gender"].ToString();
                    //userDetails.WebPass = dbReaderMDB["web_pass"].ToString();
                }
            }
            catch (Exception ex)
            {
                // No access to the user, save in logs
                // Continued to the next user
                CreateErrorLevel(userDetails.Guest_id, CurrentMethod, String.Format("Email: {0}", userDetails.Email), LoggerService.ActionType.Request);
                CreateErrorLevel(userDetails.Guest_id, CurrentMethod, ex.Message, LoggerService.ActionType.Response, "No access to the user");
                throw new Exception(ex.Message);
            }
            finally
            {
                if (cmd.Connection.State == ConnectionState.Open)
                {
                    cmd.Connection.Close();
                }
            }

            userDetails.Date_of_Birth = date.GetDayAndMonth(userDetails.Date_of_Birth);
            userDetails.Anniversary = date.GetDayAndMonth(userDetails.Anniversary);
            userDetails.Partnet_Date_of_Birth = date.GetDayAndMonth(userDetails.Partnet_Date_of_Birth);
            userDetails.End_Date = date.GetDate(userDetails.End_Date);
            userDetails.First_Enrollment_Date = date.GetDate(userDetails.First_Enrollment_Date);
            userDetails.Renew_Date = date.GetDate(userDetails.Renew_Date);
            userDetails.Enrollment_Date = date.GetDate(userDetails.Enrollment_Date);

            return userDetails;
        }

        private void GetReservationsDetails(KeyValuePair<string, string> emailUser, UserDetails userDetails)
        {
            string CurrentMethod = "GetReservationsDetails";
            dbConnFattalStats.Open();
            OdbcCommand reservations = null;
            reservations = new OdbcCommand(GetReservationsByAccountId(emailUser.Key))
            {
                Connection = dbConnFattalStats
            };

            if (reservations.Connection.State == ConnectionState.Closed)
            {
                reservations.Connection.Open();
            }

            try
            {
                dbReaderFS = reservations.ExecuteReader();
                userDetails.ResDetails = new List<ResDetails>();
                while (dbReaderFS.Read()) // max 5 times
                {
                    ResDetails resDetails = new ResDetails
                    {
                        Extra_Income = dbReaderFS["extra_income"].ToString(),
                        Gross_Room_Income = dbReaderFS["gross_room_income"].ToString(),
                        Master = dbReaderFS["master"].ToString(),
                        Adults = dbReaderFS["adults"].ToString(),
                        Childrens = dbReaderFS["children"].ToString(),
                        Babies = dbReaderFS["babies"].ToString(),
                        Hotel = dbReaderFS["hotel"].ToString(),
                        Hotel_Name = dbReaderFS["hotel_name"].ToString(),
                        Plan = dbReaderFS["plan"].ToString(),
                        Room_Nights = dbReaderFS["room_nights"].ToString(),
                        Room_Category = dbReaderFS["room_category"].ToString(),
                        Reservation_Status = dbReaderFS["reservation_status"].ToString(),
                        Nationality = dbReaderFS["nationality"].ToString(),
                        Arrival_Date = dbReaderFS["arrival_date"].ToString(),
                        Depart_Date = dbReaderFS["depart_date"].ToString(),
                        Taken_Date = dbReaderFS["taken_date"].ToString()

                        //RoomNumber = dbReaderFS["room_number"].ToString(),
                        //PriceCode = dbReaderFS["price_code"].ToString(),
                        //AgentName = dbReaderFS["agent_name"].ToString(),
                        //TakenClerkName = dbReaderFS["taken_clerk_name"].ToString(),
                        //MarketSegmentDescription = dbReaderFS["market_segment_description"].ToString(),
                        //CompanyName = dbReaderFS["company_name"].ToString(),
                        //GuestCategoryDescription = dbReaderFS["guest_category_description"].ToString(),
                        //LocalTourist = dbReaderFS["local_tourist"].ToString(),
                        //CancellationDate = dbReaderFS["cancellation_date"].ToString(),
                    };
                    userDetails.ResDetails.Add(resDetails);
                }
            }
            catch (Exception ex)
            {
                // Could not read the reservations.
                CreateErrorLevel(userDetails.Guest_id, CurrentMethod, String.Format("Email: {0}", userDetails.Email), LoggerService.ActionType.Request);
                CreateErrorLevel(userDetails.Guest_id, CurrentMethod, ex.Message, LoggerService.ActionType.Response, "No access to Reservations user");
                // Reservations array remains empty
                throw new Exception(ex.Message);
            }
            finally
            {
                if (reservations.Connection.State == ConnectionState.Open)
                {
                    reservations.Connection.Close();
                }
                dbConnFattalStats.Close();
            } 
        }

        private string GetUpdatedAccountsByUpdateDate(DateTime sd, DateTime ed)
        {
            CultureInfo ci = new CultureInfo("en-US");

            string sQuery = "select a.c_guest_id,a.c_account_id,c_email,min(r.c_timestamp),max(r.c_timestamp),min(a.c_timestamp),max(a.c_timestamp) ";
            sQuery += " from t009_reservation_summary r with (nolock)  ";
            sQuery += " inner join crm_accounts a with (nolock) on r.c_guest_id=a.c_guest_id   ";
            sQuery += " where ((r.c_timestamp >='" + sd.ToString("yyyy-MM-dd", ci) + "' and r.c_timestamp<'" + ed.ToString("yyyy-MM-dd", ci) + "') or (a.c_timestamp >='" + sd.ToString("yyyy-MM-dd", ci) + "' and a.c_timestamp <'" + ed.ToString("yyyy-MM-dd", ci) + "'))  and isnull(a.c_email,'')<>''  ";
            sQuery += " and a.c_guest_id>0";
            sQuery += " group by a.c_guest_id,a.c_account_id,c_email order by min(r.c_timestamp)  --desc";

            return sQuery;
        }

        private string GetAccountsByEmail(string email)
        {
            CultureInfo ci = new CultureInfo("en-US");

            string sQuery = "select a.c_guest_id,a.c_account_id,c_email,min(r.c_timestamp),max(r.c_timestamp),min(a.c_timestamp),max(a.c_timestamp) ";
            sQuery += " from t009_reservation_summary r with (nolock)  ";
            sQuery += " inner join crm_accounts a with (nolock) on r.c_guest_id=a.c_guest_id   ";
            sQuery += " where a.c_email='" + email + "'";
            sQuery += " and a.c_guest_id>0";
            sQuery += " group by a.c_guest_id,a.c_account_id,c_email order by min(r.c_timestamp)  --desc";

            return sQuery;
        }

        private string GetContactByGuestIdAndEmail(string GuestId, string email)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("select");
            sb.AppendLine("crm_accounts.c_guest_id as guest_id,");
            sb.AppendLine("crm_accounts.c_email as email,");
            sb.AppendLine("crm_accounts.c_first_name as first_name,");
            sb.AppendLine("crm_accounts.c_last_name as last_name,");
            sb.AppendLine("crm_accounts.c_phone as phone,");
            sb.AppendLine("crm_accounts.c_mobile as mobile,");
            sb.AppendLine("crm_accounts.c_date_of_birth as date_of_birth,");
            sb.AppendLine("crm_accounts.c_account_id as account_id,");
            sb.AppendLine("crm_accounts.c_anniversary as anniversary,");
            sb.AppendLine("crm_accounts.c_timestamp as timestamp,");
            sb.AppendLine("crm_account_clubs.c_club_member_number as club_member_number,");
            sb.AppendLine("crm_account_clubs.c_end_date as end_date,");
            sb.AppendLine("crm_account_clubs.c_first_enrollment_date as first_enrollment_date,");
            sb.AppendLine("crm_account_clubs.c_enrollment_date as enrollment_date,");
            sb.AppendLine("crm_account_clubs.c_renew_date as renew_date,");
            sb.AppendLine("crm_account_ratings.c_account_rating_description as guest_rating,");
            sb.AppendLine("crm_account_clubs.c_membership_status as membership_status,");
            sb.AppendLine("case when isnull(crm_account_clubs.c_membership_status,'')='Active' then 1 else 0 end as shamaim_registration,");
            sb.AppendLine("T005_GUEST_PROFILE.c_city as city,");
            sb.AppendLine("DBA.t005s_sms_allowed.c_send_sms_marketing as allow_sms_marketing,");
            sb.AppendLine("DBA.t005s_sms_allowed.c_send_email_marketing as allow_email_marketing,");
            sb.AppendLine("crm_accounts.c_spouse_name as spouse_name,");
            sb.AppendLine("crm_accounts.c_spouse_last_name as spouse_last_name,");
            sb.AppendLine("crm_accounts.c_spouse_date_of_birth as spouse_date_of_birth,");
            sb.AppendLine("crm_accounts.c_spouse_gender  as spouse_gender,");
            sb.AppendLine("crm_accounts.c_spouse_id as spouse_id,");
            sb.AppendLine("COUNT(t009_reservation_summary.c_nights) as count_orders_last_year,");
            sb.AppendLine("sum(t009_reservation_summary.c_nights) as Total_nights_last_year,");
            sb.AppendLine("sum(t009_reservation_summary.c_total_room_income_base_curr) as total_income_last_year,");
            sb.AppendLine("T005_GUEST_PROFILE.c_web_pass as web_pass");
            sb.AppendLine("from DBA.crm_accounts");
            sb.AppendLine("left outer join DBA.crm_account_clubs on DBA.crm_accounts.c_account_id = crm_account_clubs.c_account_id and crm_account_clubs.c_club_code='shamaim'");
            sb.AppendLine("left outer join DBA.crm_account_status on DBA.crm_accounts.c_account_status = crm_account_status.c_account_status_id");
            sb.AppendLine("left outer join DBA.t005s_sms_allowed on DBA.crm_accounts.c_guest_id = t005s_sms_allowed.c_guest_id");
            sb.AppendLine("left outer join DBA.crm_account_ratings on DBA.crm_accounts.c_rating_id = crm_account_ratings.c_account_rating_id");
            sb.AppendLine("left outer join DBA.T005_GUEST_PROFILE on DBA.crm_accounts.c_guest_id = T005_GUEST_PROFILE.c_guest_id");
            sb.AppendLine("left outer join DBA.t009_reservation_summary on DBA.T005_GUEST_PROFILE.c_guest_id = t009_reservation_summary.c_guest_id and t009_reservation_summary.c_reservation_status='chkou' and t009_reservation_summary.c_arrival_date>dateadd(mm,-12,getdate())");
            sb.AppendLine("where crm_accounts.c_guest_id='" + GuestId + "' and crm_accounts.c_email='" + email + "'");
            sb.AppendLine("group by crm_accounts.c_guest_id,");
            sb.AppendLine("crm_accounts.c_email,");
            sb.AppendLine("crm_accounts.c_first_name,");
            sb.AppendLine("crm_accounts.c_last_name,");
            sb.AppendLine("crm_accounts.c_phone,");
            sb.AppendLine("crm_accounts.c_mobile,");
            sb.AppendLine("crm_accounts.c_date_of_birth,");
            sb.AppendLine("crm_accounts.c_account_id,");
            sb.AppendLine("crm_accounts.c_anniversary,");
            sb.AppendLine("crm_accounts.c_timestamp,");
            sb.AppendLine("crm_account_clubs.c_club_member_number,");
            sb.AppendLine("crm_account_clubs.c_end_date,");
            sb.AppendLine("crm_account_clubs.c_first_enrollment_date,");
            sb.AppendLine("crm_account_clubs.c_enrollment_date,");
            sb.AppendLine("crm_account_clubs.c_renew_date,");
            sb.AppendLine("crm_account_ratings.c_account_rating_description,");
            sb.AppendLine("crm_account_clubs.c_membership_status,");
            sb.AppendLine("case when isnull(crm_account_clubs.c_membership_status,'')='Active' then 1 else 0 end ,");
            sb.AppendLine("T005_GUEST_PROFILE.c_city,");
            sb.AppendLine("DBA.t005s_sms_allowed.c_send_sms_marketing,");
            sb.AppendLine("DBA.t005s_sms_allowed.c_send_email_marketing,");
            sb.AppendLine("crm_accounts.c_spouse_name,");
            sb.AppendLine("crm_accounts.c_spouse_last_name,");
            sb.AppendLine("crm_accounts.c_spouse_date_of_birth,");
            sb.AppendLine("crm_accounts.c_spouse_gender,");
            sb.AppendLine("crm_accounts.c_spouse_id,");
            sb.AppendLine("T005_GUEST_PROFILE.c_web_pass");
            return sb.ToString();
        }

        private string GetReservationsByAccountId(string GuestId)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT Top 5");
            sb.AppendLine("max(r.guestid) as guestid,");
            sb.AppendLine("min(r.resdate) as resdate,");
            sb.AppendLine("sum(case when actual_reservation_status in ('rel','wait','del','can') then 0 else r.extra_income end)  as extra_income,");
            sb.AppendLine("sum(case when actual_reservation_status in ('rel','wait','del','can') then 0 else r.gross_room_income end) as gross_room_income,");
            sb.AppendLine("max(r.price_code) as price_code,");
            sb.AppendLine("r.master,");
            sb.AppendLine("max(r.agent_name) as agent_name,");
            sb.AppendLine("max(r.taken_clerk_name) as taken_clerk_name,");
            sb.AppendLine("max(r.market_segment_description) as market_segment_description ,");
            sb.AppendLine("max(r.company_name) as company_name ,");
            sb.AppendLine("max(case when  actual_reservation_status in ('rel','wait','del','can') then '0_' + actual_reservation_status");
            sb.AppendLine("  when actual_reservation_status='res' then '1_res'");
            sb.AppendLine("   when actual_reservation_status='arriv' then '2_arriv'");
            sb.AppendLine("   when actual_reservation_status='chkin' then '3_chkin'");
            sb.AppendLine("   when actual_reservation_status='chkou' then '4_chkou'");
            sb.AppendLine("   when actual_reservation_status='nosho' then '5_nosho'");
            sb.AppendLine("   else '0_'+actual_reservation_status");
            sb.AppendLine("   end)");
            sb.AppendLine("as reservation_status_1,");
            sb.AppendLine("sum(case when actual_reservation_status in ('rel','wait','del','can') then 0 else r.adults end)  as adults ,");
            sb.AppendLine("sum(case when actual_reservation_status in ('rel','wait','del','can') then 0 else r.children end)  as children,");
            sb.AppendLine("sum(case when actual_reservation_status in ('rel','wait','del','can') then 0 else r.babies end)  as babies,");
            sb.AppendLine("r.hotel,");
            sb.AppendLine("r.hotel_name,");
            sb.AppendLine("max(r.plan) as plan,");
            sb.AppendLine("sum(case when actual_reservation_status in ('rel','wait','del','can') then 0 else r.room_nights end ) as room_nights ,");
            sb.AppendLine("max(r.room_number) as room_number ,");
            sb.AppendLine("max(r.room_category) as room_category ,");
            sb.AppendLine("right(reservation_status_1,len(reservation_status_1)-2) as reservation_status,");
            sb.AppendLine("min(r.arrival_date) as arrival_date,");
            sb.AppendLine("max(r.cancellation_date) as cancellation_date ,");
            sb.AppendLine("max(r.depart_date) as depart_date ,");
            sb.AppendLine("max(r.taken_date) as taken_date,");
            sb.AppendLine("max(r.guest_category_description) as guest_category_description,");
            sb.AppendLine("max(r.local_tourist) as local_tourist ,");
            sb.AppendLine("max(r.nationality) as nationality");
            sb.AppendLine("from (");
            sb.AppendLine("SELECT");
            sb.AppendLine("max(r.guestid) as guestid ,");
            sb.AppendLine("min(r.resdate) as resdate,");
            sb.AppendLine("sum(r.extra_income) as extra_income,");
            sb.AppendLine("sum(r.gross_room_income*r.room_nights) as gross_room_income,");
            sb.AppendLine("max(r.price_code) as price_code,");
            sb.AppendLine("r.master,");
            sb.AppendLine("r.res,");
            sb.AppendLine("max(r.agent_name) as agent_name,");
            sb.AppendLine("max(r.taken_clerk_name) as taken_clerk_name,");
            sb.AppendLine("max(r.market_segment_description) as market_segment_description,");
            sb.AppendLine("max(r.company_name) as company_name ,");
            sb.AppendLine("max(r.adults) as adults ,");
            sb.AppendLine("max(r.children) as children,");
            sb.AppendLine("max(r.babies) as babies,");
            sb.AppendLine("r.hotel,");
            sb.AppendLine("r.hotel_name,");
            sb.AppendLine("max(r.plan) as plan,");
            sb.AppendLine("sum(r.room_nights) as room_nights ,");
            sb.AppendLine("max(r.room_number) as room_number ,");
            sb.AppendLine("max(r.room_category) as room_category ,");
            sb.AppendLine("max(case when r.actual_reservation_status is null then r.reservation_status else r.actual_reservation_status end ) as actual_reservation_status,");
            sb.AppendLine("min(r.arrival_date) as arrival_date,");
            sb.AppendLine("max(r.cancellation_date) as cancellation_date ,");
            sb.AppendLine("max(r.depart_date) as depart_date ,");
            sb.AppendLine("max(r.taken_date) as taken_date,");
            sb.AppendLine("max(r.guest_category_description) as guest_category_description,");
            sb.AppendLine("max(r.local_tourist) as local_tourist,");
            sb.AppendLine("max(r.nationality) as  nationality");
            sb.AppendLine("from BI.AVRESERVATIONS r");
            sb.AppendLine("where r.guestid=" + GuestId);
            sb.AppendLine("group by");
            sb.AppendLine("r.master,");
            sb.AppendLine("r.res ,");
            sb.AppendLine("r.hotel,");
            sb.AppendLine("r.hotel_name");
            sb.AppendLine(") as r");
            sb.AppendLine("group by");
            sb.AppendLine("r.master,");
            sb.AppendLine("r.hotel,");
            sb.AppendLine("r.hotel_name");
            sb.AppendLine("Order by max(r.taken_date) DESC");

            return sb.ToString();
        }
    }
}
