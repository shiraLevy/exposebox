﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exposebox.Services
{
    class ChangingFormat
    {
        public string GetDayAndMonth(string date)
        {
            try
            {
                DateTime dateFormat = DateTime.Parse(date);
                string d = String.Format("{0:dd MMMM}", dateFormat); // 08 May
                return String.Format("{0:dd/MM}", dateFormat);
            }
            catch
            {
                return date;
            }
        }

        public string GetDate(string date)
        {
            try
            {
                DateTime dateFormat = DateTime.Parse(date);
                return String.Format("{0:dd/MM/yyyy}", dateFormat);
            }
            catch
            {
                return date;
            }
        }
    }
}
