﻿
using System;

namespace Exposebox.Services
{
    class FileService
    {
        public string WriteLogFile(string text)
        {
            DateTime date = DateTime.Now;
            string fileName = date.ToString("MM.dd.yy_H.mm.ss");
            string path = String.Format(@"C:\Users\zigit\Documents\Visual Studio 2017\Projects\Exposrbox\Logs\{0}.txt", fileName);

            try
            {
                System.IO.File.WriteAllText(path, text);
            }
            catch (Exception ex)
            {
                return String.Format("not saving log because: {0}", ex.Message);
            }
            return path;
        }
    }
}
