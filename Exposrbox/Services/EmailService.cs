﻿using Exposrbox;
using Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Exposebox.Services
{
    class EmailService : BaseLogger
    {
        public void SendingEmail(string email, int count)
        {
            string CurrentMethod = "SendingEmail";
            CreateLogLevel("", CurrentMethod, String.Format("Sending email to: {0}", email), LoggerService.ActionType.Request);
            MailMessage mail = new MailMessage
            {
                From = new MailAddress("noreply@fattal.co.il", "דוח Exposebox")
            };
            mail.To.Add(email);
            mail.Subject = "כמות משתמשים שנשלחה ל Exposebox";
            mail.Body = EmailMessage(count);
            mail.IsBodyHtml = true;
            mail.BodyEncoding = System.Text.Encoding.UTF8;

            SmtpClient client = new SmtpClient
            {
                Host = "10.8.1.119",
                Credentials = new NetworkCredential("", "")
            };

            try
            {
                client.Send(mail);
                CreateLogLevel("", CurrentMethod, "Email Sent", LoggerService.ActionType.Response);
            }
            catch (Exception ex)
            {
                CreateErrorLevel("", CurrentMethod, "Trying to Sending summary Email", LoggerService.ActionType.Request);
                CreateErrorLevel("", CurrentMethod, ex.Message, LoggerService.ActionType.Response, "Could not sending email");
            }
        }

        private string EmailMessage(int count)
        {
            StringBuilder mass = new StringBuilder();
            mass.AppendLine(@"<style>
                div{
                    direction:rtl';
                    font-family: Assistant;
                    text-align:right;
                }
            </style>");

            mass.AppendLine("<div>");
            mass.AppendLine("כמות משתמשים שנשלחה ל Exposebox");
            mass.Append(String.Format("היא - {0}", count));
            mass.AppendLine("</div>");
            return mass.ToString();
        }
    }
}
