﻿using System;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace Logger
{
    public class LoggerService
    {
        private readonly string connectionString;
        private readonly LogLevel minLogLevel;
        OleDbConnection zigitDb;

        class LogDetails
        {
            public string UserId { get; set; }
            public DateTime DateLog { get; set; }
            public LogLevel ErrorLevel { get; set; }
            public string MethodName { get; set; }
            public string MessageLog { get; set; }
            public ActionType ActionLog { get; set; }
            public string isException { get; set; }
            public string Extras { get; set; }
        }

        public enum LogLevel
        {
            Info = 1,
            Debug = 2,
            Log = 3,
            Error = 4,
            Critical = 5
        }

        public enum ActionType
        {
            Null = 0,
            Request = 1,
            Response = 2
        }

        public LoggerService(string _connectionString, LogLevel _minLogLevel = LogLevel.Log)
        {
            connectionString = _connectionString;
            minLogLevel = _minLogLevel;
            zigitDb = new OleDbConnection(_connectionString);
        }

        public void CreateLog(string IdUser, LogLevel ErrorLevel, string MethodName, string MessageLog, ActionType ActionLog, bool isException, string IpUser, string Extras)
        {                                                         
            if ( minLogLevel <= ErrorLevel)
            {
                try
                {
                    LogDetails newLog = new LogDetails
                    {
                        UserId = IdUser,
                        DateLog = DateTime.Now,
                        ErrorLevel = ErrorLevel,
                        MethodName = MethodName,
                        MessageLog = MessageLog,
                        ActionLog = ActionLog,
                        isException = (isException) ? "1" : "0",
                        Extras = Extras
                    };
                    InsertLogToDb(newLog);
                }
                catch (Exception)
                {
                    // Add logic
                    //CreateLog(LoggerService.LogLevel.Error, "Exception - " + ex.Message, IdUser, "CreateLog");
                }
            }            
        }

        private void InsertLogToDb(LogDetails log)
        {
            string sql = "insert into ExposeBox_Log (UserId, DateLog, ErrorLevel, MethodName, MessageLog, ActionLog, isException, Extras) values (?, ?, ?, ?, ?, ?, ?, ?)";
            OleDbCommand command = new OleDbCommand
            {
                CommandText = sql,
                CommandTimeout = 20,
                Connection = zigitDb,
            };

            command.Parameters.AddWithValue("@UserId", log.UserId);
            command.Parameters.AddWithValue("@DateLog", log.DateLog);
            command.Parameters.AddWithValue("@ErrorLevel", log.ErrorLevel);
            command.Parameters.AddWithValue("@MethodName", log.MethodName);
            command.Parameters.AddWithValue("@MessageLog", log.MessageLog);
            command.Parameters.AddWithValue("@ActionLog", log.ActionLog);
            command.Parameters.AddWithValue("@isException", log.isException);
            command.Parameters.AddWithValue("@Extras", log.Extras);
            try
            {
                command.Connection.Open();
                command.ExecuteReader();
            }
            catch (Exception ex)
            {
                int p = 9; 
            }
            finally
            {
                command.Connection.Close();
            }
        }
    }
}
